# encoding:utf-8

# Decirle a Django qué modelos tiene la aplicación
from principal.models import User, Movie, Rate, Category
from django.contrib import admin

admin.site.register(User)
admin.site.register(Rate)
admin.site.register(Movie)
admin.site.register(Category)
