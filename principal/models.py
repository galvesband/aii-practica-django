# encoding:utf-8
from django.db import models

# Create your models here.
# Si surgen dudas sobre el modelo y sus relaciones, ir a doc de django
# http://djangoproject.com

# Documentación sobre el modelo:
# https://docs.djangoproject.com/en/1.11/topics/db/models/

# Nota: al definir las relaciones entre las clases,
# el primer modelo tiene que declarar una relación con otro
# modelo que aún no esta definido porque esta más abajo.
# En esos casos, el nombre del modelo se pone entre comillas
# y django lo resolverá después.

# Nota: ojo a las relaciones ManyToMany, ver doc
# Además, esto es un ejemplo de cómo utilizar el ORM y acceder a
# las distintas relaciones
# https://docs.djangoproject.com/en/1.11/topics/db/examples/many_to_many/

# Sobre la práctica, hay muchas relaciones ManyToMany


class User(models.Model):
    user_id = models.IntegerField(primary_key=True, unique=True, blank=False, null=False)
    age = models.IntegerField(blank=False, null=False)
    gender = models.CharField(blank=False, null=False, max_length=1, choices=(('M', 'Hombre'), ('F', 'Mujer')))
    occupation = models.ForeignKey('Occupation', null=True, on_delete=models.SET_NULL)
    postal_code = models.CharField(blank=False, null=False, max_length=5)
    rates = models.ManyToManyField('Movie', through='Rate', blank=True)

    def __unicode__(self):
        return unicode(self.user_id)


class Movie(models.Model):
    movie_id = models.IntegerField(primary_key=True, unique=True, blank=False, null=False)
    title = models.CharField(max_length=256, blank=False, null=False)
    release_date = models.DateField(blank=False, null=True)
    imdb_url = models.CharField(blank=False, null=False, max_length=512)
    categories = models.ManyToManyField('Category', blank=True)
    rates = models.ManyToManyField('User', through='Rate', blank=True)

    def __unicode__(self):
        return self.title


class Rate(models.Model):
    user = models.ForeignKey(User)
    movie = models.ForeignKey(Movie)
    rate = models.IntegerField()


class Category(models.Model):
    category_id = models.IntegerField(primary_key=True, unique=True, blank=False, null=False)
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name


class Occupation(models.Model):
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name
