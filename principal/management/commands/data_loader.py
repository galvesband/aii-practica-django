# encoding:utf-8

import os
from datetime import date
from django.db import transaction
from django.core.management.base import BaseCommand
from principal.models import Category, Movie, Rate, User, Occupation


# Lo normal para ejecutar esto es hacer un reset de la DB y tal antes:
# Reset, pedirá nuevo super-user:
# $ python2 manage.py reset principal
# Cargar los datos:
# $ python2 manage.py data_loader
class Command(BaseCommand):
    help = 'Carga datos del dataset en la aplicación. Recomendado "reset" previo.'

    DATA_DIR = 'ml-100k'
    data_dir = False

    # Mapa de ocupaciones: nombre => Occupation
    map_occupations = {}
    # Mapa de géneros: genre_id => Genre
    map_categories = {}
    # Mapa de películas: movie_id => Movie
    map_movies = {}
    # Mapa de usuarios: user_id => User
    map_users = {}

    def _process_file(self, file_path, part_count, separator, functor):
        """ Abre y procesa un archivo. Cada línea es dividida en partes con el separador
         pasado como argumento. Si el número de partes de la línea no coincide con el esperado
         ignorará la lína. Cada línea con el número correcto de partes se le pasará al functor. """
        file_path = os.path.dirname(os.path.abspath(__file__)) + "/" + self.DATA_DIR + "/" + file_path
        f = open(file_path, "r")

        for line in f:
            line = line.strip().replace("\n", "")
            if line == "":
                continue

            # Convertir línea a unicode
            line = unicode(line, errors='ignore')

            parts = line.split(separator)
            if len(parts) != part_count:
                self.stderr.write(u"- ERROR: número de partes incorrecto (%s) separando con '%s' en línea, ignorando: "
                                  u"%s\n" % (len(parts), separator, line))
                continue

            # Crear objeto, etc
            functor(parts)

    def _functor_occupation(self, parts):
        occupation = Occupation()
        occupation.name = parts[0]
        occupation.save()

        self.map_occupations[occupation.name] = occupation
        self.stdout.write(" - %s\n" % occupation.name)

    def _functor_category(self, parts):
        cat = Category()
        cat.name = parts[0]
        cat.category_id = int(parts[1])
        cat.save()

        self.map_categories[cat.category_id] = cat
        self.stdout.write(" - %s (%s)\n" % (cat.name, str(cat.category_id)))

    def _functor_user(self, parts):
        user = User()
        user.user_id = int(parts[0])
        user.age = int(parts[1])
        user.gender = parts[2]
        user.occupation = self.map_occupations[parts[3]]
        user.postal_code = parts[4]
        user.save()

        self.map_users[user.user_id] = user
        self.stdout.write(" - %s (%s)\n" % (user.user_id, user.age))

    def _functor_movie(self, parts):
        movie = Movie()
        movie.movie_id = int(parts[0])
        movie.title = parts[1]
        try:
            movie.release_date = self._parse_date(parts[2])
        except IndexError:
            self.stderr.write(u"Fecha inválida, ignorando: %s\n" % parts[2])
            pass
        movie.imdb_url = parts[4]

        base_index = 5
        for i in range(0, 19):
            if parts[base_index + i] == "1":
                movie.categories.add(self.map_categories[i].category_id)
        movie.save()

        self.map_movies[movie.movie_id] = movie
        self.stdout.write(" - %s\n" % movie.title)

    def _functor_rate(self, parts):
        rate = Rate()
        rate.user = self.map_users[int(parts[0])]
        rate.movie = self.map_movies[int(parts[1])]
        rate.rate = int(parts[2])
        rate.save()

        self.stdout.write(" - %s - %s - %s\n" % (rate.user.user_id, rate.movie.title, str(rate.rate)))

    @staticmethod
    def _parse_date(date_string):
        months = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10,
                  'Nov': 11, 'Dec': 12}
        date_parts = date_string.split("-")
        rval = date(int(date_parts[2]), months[date_parts[1]], int(date_parts[0]))

        return rval

    def handle(self, *args, **options):
        self.data_dir = os.path.dirname(os.path.abspath(__file__)) + "/" + self.DATA_DIR
        self.stdout.write("Cargando datos de %s\n" % self.data_dir)

        # En una transacción, para que SQLite no tenga que sincronizar
        # la base de datos después de cada inserción. De esta forma
        # lo hará de golper al hacer commit
        with transaction.commit_on_success():
            self.stdout.write("Cargando ocupaciones...\n")
            self._process_file("u.occupation", 1, "|", self._functor_occupation)

            self.stdout.write("Cargando categorias (géneros)...\n")
            self._process_file("u.genre", 2, "|", self._functor_category)

            self.stdout.write("Cargando usuarios...\n")
            self._process_file("u.user", 5, "|", self._functor_user)

            self.stdout.write("Cargando películas...\n")
            self._process_file("u.item", 24, "|", self._functor_movie)

            self.stdout.write("Cargando puntuaciones...\n")
            self._process_file("u.data", 4, "\t", self._functor_rate)
