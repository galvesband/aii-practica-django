# encoding:utf-8
from django.core.validators import MaxValueValidator, MinValueValidator
from django.forms import ModelForm
from django import forms
from principal.models import Movie


class SearchMovieForm(forms.Form):
    year = forms.IntegerField(
        label=u'Año',
        required=True,
        help_text=u'Año en el que buscar películas.',
        validators=[
            MaxValueValidator(2017),
            MinValueValidator(1900)
        ]
    )
