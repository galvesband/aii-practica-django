# Create your views here.
# from django.contrib.auth.decorators import login_required
from django.db.models import F
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.db.models import Count, Avg, Sum
from principal.models import Movie, User, Category
from principal.forms import SearchMovieForm


def home(request):
    return render_to_response('home.html', {}, context_instance=RequestContext(request))


def movies(request):
    return render_to_response('movies.html', {
        'movies': Movie.objects.all()
    }, context_instance=RequestContext(request))


def categories(request):
    return render_to_response('categories.html', {
        'categories': Category.objects.all()
    }, context_instance=RequestContext(request))


def users(request):
    data = User.objects.values('postal_code').annotate(user_count=Count('user_id'))
    return render_to_response('users.html', {
        'user_data': data
    }, context_instance=RequestContext(request))


def search_movie_year(request):
    movies_found = []
    if request.method == 'POST':
        search_form = SearchMovieForm(request.POST)
        if search_form.is_valid():
            movies_found = Movie.objects.filter(release_date__year=search_form.cleaned_data['year'])
    else:
        search_form = SearchMovieForm()

    return render_to_response('search_movie.html', {
        'search_form': search_form,
        'movies': movies_found
    }, context_instance=RequestContext(request))


def search_best_movie_year(request):
    movie_found = None
    if request.method == 'POST':
        search_form = SearchMovieForm(request.POST)
        if search_form.is_valid():
            # Hay que buscar peliculas de un anyo dado y agregar la media
            # de rates de ellas, ordenarlas por esa media (descendente) y
            # quedarse con el primer resutlado
            movie_found = Movie.objects.filter(release_date__year=search_form.cleaned_data['year']) \
                .annotate(score=Avg('rate__rate')).order_by('-score')[:1]
    else:
        search_form = SearchMovieForm()

    return render_to_response('search_best_movie.html', {
        'search_form': search_form,
        'movie_found': movie_found
    }, context_instance=RequestContext(request))
