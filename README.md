# Ejercicio de Django #

Si traemos el ejercicio hecho después de feria ya tenemos un 6 y
el resto será ampliar nota.

## Sobre el ejercicio ##

Usuario y película y alguna otra cosa tienen ids que ya vienen en el
dataset incluído en la info. del ejercicio.

Nos pide modelo y plantillas para mostrar películas y tal. Lo que
más le interesa es que creemos el modelo y lo llenemos de datos
con el dataset. La parte más complicada es la extracción de datos
e introducción en la BD.

El dataset trae un README con info. Lo que más nos interesa es:

 - u.info
 - u.data: 100mil puntuaciones de 943 usuariosa 1682 películas.
 - u.item: peliculas (id, title, release_date, video_release_date
   (el profe no lo pide), imdb url (no funciona) y otros 19 campos
   booleanos representando géneros indicando en cada caso si la
   película pertenece a ese género.
 - u.genre: géneros de películas
 - u.user

Cada archivo viene con un registro por línea, cada campo separado
del resto por '|'. Ver el README para más info.